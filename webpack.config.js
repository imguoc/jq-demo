const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const ExtractTextPlugin = require("extract-text-webpack-plugin")

module.exports = {

    mode: 'development',

    entry: {
        main: './src/main.js',
    },

    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/'
    },

    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: 'css-loader'
                })
                // use: [
                //     'style-loader',
                //     'css-loader'
                // ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.(csv|tsv)$/,
                use: [
                    'csv-loader'
                ]
            },
            {
                test: /\.xml$/,
                use: [
                    'xml-loader'
                ]
            },
            {
                // https://github.com/aui/art-template-loader
                test: /\.art$/,
                loader: "art-template-loader",
                options: {
                    // art-template options (if necessary)
                    // @see https://github.com/aui/art-template
                }
            }
        ]
    },

    devtool: 'inline-source-map',

    devServer: {
        contentBase: __dirname + '',
        watchContentBase: true,
        port: 8888,
        // hot: true,
    },

    plugins: [
        
        // https://github.com/jaketrent/html-webpack-template
        new HtmlWebpackPlugin({
            template: './index.html'
        }),
        // https://www.npmjs.com/package/clean-webpack-plugin
        new CleanWebpackPlugin(),
        // https://webpack.js.org/plugins/extract-text-webpack-plugin/
        new ExtractTextPlugin({
            filename: "./src/assets/css/[name].[contenthash].css",
        }),

    ],

}