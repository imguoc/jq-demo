import 'normalize.css'
import './assets/css/common.css'
import './filter'

import $ from "jquery"
import { hello } from './template'

let data= {
    title: 'hello world',
    list: [
        {
            name: 'title1',
            content: 'content1',
        },
        {
            name: 'title2',
            content: 'content2',
        },
        {
            name: 'title3',
            content: 'content3',
        },
    ]
}

$(() => {

    $(document.body).append(hello(data))
    
})